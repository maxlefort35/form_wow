<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="CSS/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;700&display=swap" rel="stylesheet">
</head>
<body>

<div class="ecran">
    <div class="carte_titre">
        <h1>Et toi, à quoi tu joues ?</h1>
    </div>

    <h2>Où aimes-tu te situer pendant le combat ?</h2>
    
        <form id="formulaire" action="" method="get">
        <section class="presentation">
            <div class="carte">
            <img src="image/cac.jpg" alt="">
                <div class="description">
                <h2>Dans la mélée</h2>
                    <p>Tu aimes être au cœur de l'action, et brandir ton arme contre l'ennemie !</p>
                    <input type="checkbox" value="melee" name="role_melee" id="melee">
                </div>    
            </div>

            <div class="carte">
            <img src="image/cast.jpg" alt="">
                <div class="description">
                    <h2>A distance</h2>
                    <p>Tu aimes avoir une vue d'ensemble afin d'élaborer la meilleure stratégie pour détruire tes ennemis !</p>
                    <input type="checkbox" value="cast" name="role_cast" id="heal">
                </div>    
            </div>

            <div class="carte">
                <img src="image/poly.jpg" alt="">
                <div class="description">
                    <h2>Polyvalent</h2>
                    <p>J'aime être au cœur de l'action, mais avoir le choix de prendre mes distances !</p>
                    <input type="checkbox" value="polyvalent" name="role_polyvalent" id="dps">
                </div>    
            </div>

        </section>

        <h2>Es-tu plus :</h2>

        <section class="presentation">
            <div class="carte">
            <img src="image/nature.jpg" alt="">
                <div class="description">
                <h2>Proche de nature</h2>
                    <p>Tu es en accord avec mère-nature, préserver toute forme de vie est ta maxime.</p>
                    <input type="checkbox" value="nature" name="role_nature" id="melee">
                </div>    
            </div>

            <div class="carte">
            <img src="image/lumiere.jpg" alt="">
                <div class="description">
                    <h2>Proche de la lumière</h2>
                    <p>Pour toi le courage, c'est bien, la justice, c'est mieux ! "Calogronan -KAA".</p>
                    <input type="checkbox" value="lumiere" name="role_lumiere" id="heal">
                </div>    
            </div>

            <div class="carte">
                <img src="image/spiri.jpg" alt="">
                <div class="description">
                    <h2>Proche de la spiritualité</h2>
                    <p>Tu aimes la méditation et les plaisirs simples de la vie !
</p>
                    <input type="checkbox" value="spiritualite" name="role_spiritualite" id="dps">
                </div>    
            </div>

        </section>

        <h2>Avec quel type d'armure préfères-tu te battre ?</h2>

        <section class="presentation">
            <div class="carte">
            <img src="image/lourde.jpg" alt="">
                <div class="description">
                <h2>Armures lourdes</h2>
                    <p>Armure solide pour me protéger des coups de l'ennemi.</p>
                    <input type="checkbox" value="lourde" name="role_lourde" id="melee">
                </div>    
            </div>

            <div class="carte">
            <img src="image/cuir_maille.jpg" alt="">
                <div class="description">
                    <h2>Armures polyvalentes</h2>
                    <p>Armure assez résistante qui suit le mouvement sans gêner le mouvement ! "Urgan l'homme goujon -KAA".</p>
                    <input type="checkbox" value="poly" name="role_poly" id="heal">
                </div>    
            </div>

            <div class="carte">
                <img src="image/legere.jpg" alt="">
                <div class="description">
                    <h2>Armures légères</h2>
                    <p>Armure fluide, idéal pour les lanceurs de sorts qui veulent un style irréprochable.</p>
                    <input type="checkbox" value="legere" name="role_legere" id="dps">
                </div>    
            </div>

        </section>

            <button id="goto" class="valide" type="submit" name="submit">VALIDER</button>   
        </form> 
</div>


        <div class="reponse">
            <h1 id="nom_role"></h1>
            <img id="image_role" src="" alt="">
            <p id="description_role"></p> 
            <span id="resultat"></span>
        </div>




<script src="JS/jquery.js"></script>
<script src="JS/main.js"></script>


</body>
</html>